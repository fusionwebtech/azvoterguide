Design and development by factor1
http://www.factor1studios.com

/* TEAM */
Design & Strategy: Matt Adams
Twitter: @mattada
Location: Tempe AZ

Code development: James R. Latham
Location: Seattle WA
                       
                            
/* SITE */
Standards: HTML5, CSS3
Components: Modernizr, jQuery, Foundation
Software: Wordpress
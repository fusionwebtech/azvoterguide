<?php get_header(); ?>


<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><center>Arizona Voter Guide Blog</center></h1>
        </div>
    </div>
</section>


<section class="page-content footerbars">
<article class="content row">
<div class="large-8 large-centered columns">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
			<?php if(has_post_thumbnail()) {
			the_post_thumbnail();
			} else {	}
			?>


			<div class="entry">
				<?php the_content(); ?>
			</div>

		</div>

	<?php endwhile; ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
</div>
</article>
</section>

<?php get_footer(); ?>
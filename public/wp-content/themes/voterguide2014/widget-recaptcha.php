<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget'
 };
 </script>

<div id="recaptcha_widget" class="row" style="display:none">

    <div class="small-12">
        <div id="recaptcha_image"></div>
        <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>
    </div>

    <div class="small-12">
        <label>
            <span class="recaptcha_only_if_image">Enter the words above:</span>
            <span class="recaptcha_only_if_audio">Enter the numbers you hear:</span>
        </label>
    </div>

    <div class="small-12">
        <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
    </div>

    <div class="small-12">
        <ul class="button-group [radius round]">
            <li><a id="recaptcha_reload_btn" data-tooltip class="tiny button" title="Get a new challenge" onclick="Recaptcha.reload();"><i class="fa fa-refresh fa-2x"></i></a></li>
            <li><a id="recaptcha_switch_audio_btn" data-tooltip class="tiny button recaptcha_only_if_image" title="Get an audio challenge" onclick="Recaptcha.switch_type('audio');"><i class="fa fa-microphone fa-2x"></i></a></li>
            <li><a id="recaptcha_switch_img_btn" data-tooltip class="tiny button recaptcha_only_if_audio" onclick="Recaptcha.switch_type('image');"><i class="fa fa-picture-o fa-2x"></i></a></li>
            <li><a id="recaptcha_whatsthis_btn" data-tooltip class="tiny button" title="Help" onclick="Recaptcha.showhelp();"><i class="fa fa-question fa-2x"></i></a></li>
        </ul>
    </div>

</div>

<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=<?php echo RECAPTCHA_PUBLIC_KEY; ?>"></script>
<?php
/*
Template Name: Candidate List
*/

$query = $wp_query->query;

$data_years = vgc_get_all_years();
$latest_year = end($data_years);

if(!$data_years||!array_key_exists("post_type",$query)||$query['post_type']!=="vgc_candidate") {
    header("Location: /");
    exit;
}
if(!array_key_exists("vgc_year",$query)) {
    header("Location: /candidates/".$latest_year."/");
    exit;
}

$category = vgc_get_post_category();
$link_list = vgc_get_candidate_survey_type_list();

if(count($link_list)===1) {
    header("Location: ".$link_list[0]->link);
    exit;
}

get_header();
$breadcrumbs = voterguide_get_breadcrumbs();

$post = vgc_get_first_post([
    'name'=>'candidate-list',
    'post_type'=>'page',
    'post_status'=>'publish',
]);

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1 class="small-text-center medium-text-left">List of Candidates</h1>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="candidate-names footerbars">
    <div class="row">
        <div class="medium-3 large-4 columns">

            <?php //get_template_part("candidate-search-form"); ?>

            <ul class="small-block-grid-1 large-block-grid-1">
                <?php foreach($link_list as $item) { ?>
                <li class="small-text-center medium-text-left"><a href="<?php echo esc_html($item->link); ?>"><?php echo esc_html($item->name); ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="medium-9 large-8 columns">
            <?php if($post) { ?>
                <?php echo apply_filters('the_content',$post->post_content); ?>
                <?php if($img_url=vgc_get_ad_image_url($post,"ad_image",[600,300])) { ?>
                <div class="panel">
                    <a href="#" data-href="" class="new-window">
                        <img src="<?php echo esc_html($img_url); ?>" alt="" title="" />
                    </a>
                </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>

<?php

get_footer();
function addToFavorites()
{
    if(window.sidebar) {
        if(window.sidebar.addPanel) {
            window.sidebar.addPanel(document.title,location.href,"");
        }
    } else if(window.external && window.external.AddFavorite) {
        window.external.AddFavorite(location.href, document.title);
    } else if(window.opera && window.print) {
        this.title=document.title;
    } else {
        alert("Sorry, Your browser does not support this function.\nTry Pressing CRTL+D or Command+D (Mac)");
    }
}

(function($) {

    $(document).ready(function() {

        $(".new-window[data-href]").click(function(e) {
            e.preventDefault();
            if(href=$(this).attr("data-href")) {
                window.open(href);
            }
        });

        var html$ = $("html");
        var body$ = $("body");
        var hasTips$ = $("span.question-tip[data-title]");

        var tooltipModal$ = $(".question-tip-modal");
        tooltipModal$
            .addClass("question-tip-modal")
            .click(function() { tooltipModal$.fadeOut(); })
            ;

        var getBodyMargins = function() {

            var htmlMarginTop = (!isNaN(m=parseInt(html$.css("margin-top")))) ? m : 0;
            var htmlMarginLeft = (!isNaN(m=parseInt(html$.css("margin-left")))) ? m : 0;
            var bodyMarginTop = (!isNaN(m=parseInt(body$.css("margin-top")))) ? m : 0;
            var bodyMarginLeft = (!isNaN(m=parseInt(body$.css("margin-left")))) ? m : 0;

            var htmlPaddingTop = (!isNaN(m=parseInt(html$.css("padding-top")))) ? m : 0;
            var htmlPaddingLeft = (!isNaN(m=parseInt(html$.css("padding-left")))) ? m : 0;
            var bodyPaddingTop = (!isNaN(m=parseInt(body$.css("padding-top")))) ? m : 0;
            var bodyPaddingLeft = (!isNaN(m=parseInt(body$.css("padding-left")))) ? m : 0;

            return {
                top: (htmlMarginTop+bodyMarginTop)+(htmlPaddingTop+bodyPaddingTop),
                left: (htmlMarginLeft+bodyMarginLeft)+(htmlPaddingLeft+bodyPaddingLeft)
            };
        };

        hasTips$.css("cursor","help").click(function(e) {
            e.preventDefault();

            var target$ = $(e.currentTarget);

            margins = getBodyMargins();

            tooltipModal$.children("div").html(target$.attr("data-title"));

            if($(window).width()<500) {

                tooltipModal$
                    .css("top",(margins.top+target$.offset().top+target$.outerHeight()+10)+"px")
                    .css("left","50%")
                    .fadeIn()
                    ;

            } else {

                tooltipModal$
                    .css("top",(margins.top+target$.offset().top+target$.outerHeight()+10)+"px")
                    .css("left",(margins.left+target$.offset().left+(target$.outerWidth()/2))+"px")
                    .fadeIn(200,function() {
                        var endX = tooltipModal$.offset().left+tooltipModal$.outerWidth();
                        var adjust = 0;
                        if(tooltipModal$.offset().left<0) {
                            adjust = Math.abs(tooltipModal$.offset().left)+10;
                        }
                        if(endX>$(window).width()) {
                            adjust = (endX - $(window).width() + 10) * -1;
                        }
                        if(adjust !== 0) {
                            var tipLeft = parseInt(tooltipModal$.css("left"),10);
                            tooltipModal$.css("left",(tipLeft+adjust)+"px");
                        }
                    })
                    ;

            }

        });

        candSearch$ = $("form.candidate-search");

        candSearch$.submit(function(e) {
            e.preventDefault();
            if(year=candSearch$.data("year")) {
                if(s=candSearch$.find("input").val()) {
                    location.href = '/candidates/'+year+'/search/'+s;
                }
            }
        });

        candSearch$.find(".button").click(function(e) {
            e.preventDefault();
            candSearch$.submit();
        });

        $("section.candidate [data-toggle]").click(function() {
            $($(this).data("toggle")).toggle();
        });

    });

})(jQuery);
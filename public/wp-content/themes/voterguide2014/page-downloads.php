<?php
/*
Template Name: Downloads
*/

get_header();

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><?php echo the_title(); ?></h1>
        </div>
    </div>
</section>

<section class="page-content footerbars">
    <div class="row">
        <div class="large-12 columns">
            <?php the_content(); ?>


            <?php if(get_field('downloadable_items')) { ?>
            <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4 downloadable_items">
                <?php while(has_sub_field('downloadable_items')) { ?>
                <li>
	                <a href="<?php the_sub_field('download_file');?>">
	                	<img src="<?php the_sub_field('download_thumbnail');?>">
	                	<p><?php the_sub_field('title');?></p>
	                </a>
                </li>
                <?php } ?>
            </ul>
            <?php } ?>


        </div>
    </div>
</section>


<?php

get_footer();
